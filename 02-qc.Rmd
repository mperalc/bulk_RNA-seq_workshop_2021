# Quality Control {#qc}

Here we will perform an initial quality control of the data, by looking into the sample information and metadata, performing filtering of lowly expressed genes, normalising and having a look at the principal components.

We will start by loading the dataset of interest from the *airway* package. 

```{r }
data(airway)
airway 
```

You can see that the data object is a "RangedSummarisedExperiment", which is one of the many formats in which count data (such as bulk RNA-seq) can be stored in R. It can contain different levels of sample and count information, which you can access with the following functions.

`metadata()` to get various metadata:
```{r }
metadata(airway) # Will get you high-level information e.g. about the paper where this data was published
metadata(rowRanges(airway)) # Will give you information about how the gene counts were generated
```

`assay()` will give you the raw counts. The row names are genes (represented as ENSEMBL gene IDs), and the column names are samples.
```{r }
head(assay(airway))
```

`colData()` to get the sample information, including "cell" (cell line) and "dex" (dexamethasone treatment -trt- or control -untr).
```{r}
head(as.data.frame(colData(airway)))
```


We can have a look at some metrics related to the number of reads sequenced in the experiment:
```{r }
summary(colSums(assay(airway)))

```
You can see here that there were ~22 million reads sequenced on average. If we have a look per gene and sample:
```{r }
summary(assay(airway))

```
You can see that there is a considerable difference between the median (0) and the mean value (2738), because a few genes present much higher expression, while the majority have a moderate to low expression (or no expression). It might be easier to visualize with a histogram:
```{r }
hist(assay(airway), freq = T, breaks = 200000,xlim = c(0,100), xlab = "Gene counts")

```
\n

This bulk RNA-seq count distribution is clearly not normal. It is usually modeled as a Negative Binomial distribution, which will be important for differential expression and other analyses that will be covered later.

Now we could choose between several different packages to analyse the data, including the very widely used *edgeR*, *limma* and *DESeq2*. We will proceed with *DESeq2* because it's easier to use, although *limma* provides tools to make more complex comparisons in differential expression and different normalisation approaches. You can see a good tutorial of how to use *limma* for bulk RNA-seq analysis [here](https://f1000research.com/articles/5-1408/v3).

## Building the DESeqDataSet object

To use *DESeq2* for differential expression, we need to build a *DESeqDataSet* object. *DESeq2* provides specific functions for this, including `DESeqDataSet()` which builds it directly from the *RangedSummarisedExperiment* object. We will need to specify in "design" the covariates that we will be interested in testing later during differential expression. Those will be mainly the treatment ("dex"), but we want to also control for the cell line effects ("cell"). The command would look like this:

```{r}
dds = DESeqDataSet(airway, design = ~ cell + dex)

```
We could create this object directly from matrices / data frames (for example, if we had initially just the gene count matrix and the sample information in two separate files, as is usually the case). We could then build it like this:

```{r}
# Gathering the count and sample info from the RangedSummarisedExperiment object
gene_counts = assay(airway)
sample_info = colData(airway)
dds = DESeqDataSetFromMatrix(countData = gene_counts,
                                      colData = sample_info,
                                      design = ~ cell + dex)

```
You can now examine the dds object:

```{r}
dds
```
You can see its dimension directly with `dim()`, access the raw counts with `counts()`, and the sample metadata again with `colData()`.
```{r}
dim(dds)
head(counts(dds))
```

## Filtering lowly expressed genes

The table of counts initially has `r nrow(assay(airway))` genes. We want to remove genes where the counts across all samples are zero or very low. This will speed up the analysis as well as removing genes that will have low chances of being differentially expressed. There are many sound ways of performing this filter: we can for example retain genes with at least 1 count per million (CPM) in at least two samples. 
```{r }

dds = dds[ rowSums(edgeR::cpm(counts(dds)) > 1)>=2, ]
nrow(dds)
```

You could also use the raw counts (then the filtering will be less stringent), but the threshold of 1 count or CPM is very widespread.

## Normalization

As samples are sequenced to different depths, 100 read counts in one sample are not equivalent to another. It is then necessary to correct for this variability to make biologically-sound comparisons between samples. As we have also seen before, raw count distributions follow a negative binomial or Poisson distribution and so direct comparison between groups with this model would violate the assumptions of differential expression and dimensional reduction methods such as principal component analysis.  Therefore they need to either be log-transformed (e.g. for visualization) or the count distribution needs to be taken into account for the differential expression. In addition, lowly expressed genes tend to have higher and more unreliable dispersion among replicates than genes with higher counts.
For this reason we need to model correctly the gene expression variance for differential expression (to prevent an inflation of significance for genes that have either very high or very low counts), for dimensionality reduction methods, and other analyses. 

Some simple normalisation methods are fairly straightforward: count per million (CPM), reads per kilobase per million (RPKM), etc. But these methods present limitations and are only adequate for the most simple assessments. Differential expression analysis packages provide their own methods which model the distribution of the raw counts and the mean-variance relationship, and correct for library size and composition. You can read a discussion of the different methods in this review by [Conesa et al. (2016)](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0881-8).

For visualization purposes you can use simpler normalisation methods such as log-transformation, But, to remove the dependence of the variance from the mean, we can use methods that incorporate count dispersion modelling such as the variance stabilizing transformation (VST) or the regularised logarithm which are provided by DESeq2.

```{r}
dds_rlog = rlog(dds,blind = FALSE)
dds_vst = vst(dds,blind = FALSE)
head(assay(dds_rlog), 3)
```
You can see that the resulting counts after applying these methods are very similar:
```{r}
head(assay(dds_rlog), 3)
```
```{r}
head(assay(dds_vst), 3)
```
Both `rlog()` and `vst()` normalise with respect to library size, but rlog is less sensitive to it in the case that it varies vastly across samples. You also need to set the "blind" parameter, which means that the transformations are unaffected (if TRUE) or affected (if FALSE) by the experimental design you defined before when creating the DESeq object. You should set it to FALSE if you plan to do downstream analysis where the design information is important. You can find more information in the *Blind dispersion estimation* of the [DESeq2 manual](http://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#blind-dispersion-estimation). We can compare their behavior to the simple log2 transformation of raw counts (after estimating size factors to account for sequencing depth).

```{r}
dds = estimateSizeFactors(dds)
meanSdPlot(assay(normTransform(dds))) #  log2(n+1) of raw counts
meanSdPlot(assay(dds_rlog))
meanSdPlot(assay(dds_vst))

```
\n
\n

In the y axis you can see that in general the `rlog()` and `vst()` transformations reduce the variability of the counts (they present lower standard deviation), particularly for genes with low means. You can also see from the trend lines that the `vst()` seems to provide a slightly more stable standard deviation across all the count values (here the x axis is the rank of the mean value across conditions - a lower rank means higher average expression). This is theoretically more desirable but real variability across samples due to biological conditions (and not technical issues) can lead to higher standard deviation, and therefore we should not try to force an uniform distribution. 

You can also see the effect of these transformations when doing pairwise gene count comparisons between samples:

```{r}
par( mfrow = c( 1, 3 ) )
plot(assay(normTransform(dds)),
      pch=16, cex=0.3)
plot(assay(dds_rlog)[,1:2],
      pch=16, cex=0.3)
plot(assay(dds_vst)[,1:2],
      pch=16, cex=0.3)
```    
\n
\n

The `rlog()` produces more similar counts between samples for genes with lower expression, which are usually less biologically interesting.

## Dimensionality reduction 

We intuitively understand the concept of genetic distances, for example when we say that parents and offspring share more genetic information (they are "genetically closer") than two random individuals in the population. This distance is based on a vast amount of positions in the genome, which will be identical in a higher percentage for those that are more closely related. The same logic applies in RNA-seq matrices: samples that present a higher correlation of counts per gene (after accounting for technical issues such as batch and sequencing depth) are transcriptomically closer together. 

The issue is then how to represent this distance when there are as many dimensions as genes which were sequenced. There are methods that transform these vast number of dimensions to just two or three, trying to limit the loss of information that inevitably arises from reducing complexity. The most widely used is principal component analysis (PCA). In PCA, the samples are projected onto an axis which captures most of the variance across all samples - that would be the first principal component (PC1). Then we project the samples across a second axis, orthogonal (i.e. perpendicular, uncorrelated) to the first one, which captures the vector of second highest variance (PC2). You can go on calculating vectors orthogonal to the last one, each one representing increasingly less variance. For visualization purposes we are usually interested in those that capture the highest variance. You can find an intuitive in-depth explanation of the mathematical process [here](https://stats.stackexchange.com/questions/2691/making-sense-of-principal-component-analysis-eigenvectors-eigenvalues), and a video [here](https://www.youtube.com/watch?v=_UVHneBUBW0).

Keep in mind that, because PCA is very sensitive to the variance, you should normalise the data before performing this analysis (as we have done). 

```{r}


getPCs = function(dds){
  
  df = assay(dds)
  pca = prcomp(t(df), retx = TRUE)
  
  percentVar = (pca$sdev)^2 / sum(pca$sdev^2)
  percentVar = round(100 * percentVar)
  pcs = as.data.frame(pca$x)
  pcs = cbind(pcs,colData(dds))
  pcs = as.data.frame(pcs)
  pcs = list(pcs, percentVar)
  
  names(pcs) = c("pcs","percentVar")
  return(pcs)
}
pcadata_rlog = getPCs(dds_rlog)
pcadata_vst = getPCs(dds_vst)



ggplot(pcadata_rlog$pcs, aes(PC1, PC2, color=dex, shape=cell)) +   geom_point(size=3) +
   xlab(paste0("PC1: ",pcadata_rlog$percentVar[1],"% variance")) +
   ylab(paste0("PC2: ",pcadata_rlog$percentVar[2],"% variance")) + theme_bw()

ggplot(pcadata_vst$pcs, aes(PC1, PC2, color=dex, shape=cell)) +   geom_point(size=3) +
   xlab(paste0("PC1: ",pcadata_vst$percentVar[1],"% variance")) +
   ylab(paste0("PC2: ",pcadata_vst$percentVar[2],"% variance")) + theme_bw()

``` 
\n

\n
As you can see the normalisation methods cause slightly different results.
You can also see which genes have the most influence in each PC by inspecting the *loadings*. Let's have a look at the first PC:

```{r}
getLoadings = function(dds){
  
  df = assay(dds)
  pca = prcomp(t(df), retx = TRUE)
  
  return(pca$rotation)
}

loadings_rlog = getLoadings(dds_rlog) %>% as.data.frame()
# Annotating gene names
loadings_rlog$symbol = mapIds(org.Hs.eg.db,
		        keys=row.names(loadings_rlog),
		        column="SYMBOL",
		        keytype="ENSEMBL",
		        multiVals="first")

# show the top 10 genes from PC1
loadings_rlog %>% 
  # select only the PCs we are interested in
  dplyr::select(symbol, PC1) %>%
  # convert to "long" format
  pivot_longer(cols = "PC1", names_to = "PC1", values_to = "loadings") %>% 
  # for PC1
  group_by(PC1) %>% 
  # arrange by descending order
  arrange(desc(abs(loadings))) %>% 
  # take the 10 top rows
  slice(1:10) %>%
  pull(symbol)

  
```
  
<details>
  <summary>**Exercises**</summary>
  
* Have a look at the PCA plots above. Which covariate has the largest influence in the variance, treatment ("dex") or cell line ("cell")?
* Do the samples cluster as expected? Are there any outliers?
* Which are the top 20 genes with the highest influence in separating the cell lines?
* Try applying a more stringent filtering (say, with a minimum of 5 CPMs in at least 3 samples). How did the results change?

  
  </details>
\  