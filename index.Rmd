--- 
title: "Bulk RNA-seq tutorial"
author: "Marta Pérez Alcántara"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "This is a bulk RNA-seq tutorial."
---


# Prerequisites

This workshop aims to briefly cover the most common approaches for performing bulk RNA-seq analysis. We will use a data set containing four primary human airway smooth muscle (ASM) cell lines, 4 untreated and another 4 treated with dexamethasone (a glucocorticoid). The gene counts are available through the [airway](https://bioconductor.org/packages/release/data/experiment/html/airway.html) package. You can find more information about the experiment that originated the data in the paper from [Himes and colleagues](https://pubmed.ncbi.nlm.nih.gov/24926665/).

This workshop is based on several pipelines available online including:
[the hbctraining differential expression workshop](https://github.com/hbctraining/DGE_workshop/tree/master/lessons), the pipeline 
["RNA-Seq workflow: gene-level exploratory analysis and differential expression"](https://f1000research.com/articles/4-1070/v2), and the vignettes of the various packages used here.

Let's get started!


## Load R libraries
We'll begin by loading all the necessary libraries that we need to complete our analysis. We are also using R version 4.0.2. Check your R and library versions running `sessionInfo()`.

```{r , warning=F, message=F}
# For all parts
library(DESeq2)
library(airway)
library(edgeR) # for some helper functions
library(tidyverse) # For ggplot2 and easy manipulation of data
library(patchwork) # To combine plots
library(vsn) # Some visualizations
library(AnnotationDbi) # gene annotation
library(org.Hs.eg.db) # gene annotation
library(pheatmap) # heatmaps
library(ggrepel) # repel labels
library(sva) # remove unwanted variation
library(MuSiC) # cell type proportion estimation
library(WGCNA) # weighted gene correlation network analysis
library(Seurat)

options(stringsAsFactors = FALSE) # Set this to deactivate the automatic read-in of characters as factors

```


```{r , echo=FALSE, include=F, eval=F}
# This is for building the book, no need to run it
# install.packages(c("bookdown", "knitr"))
library(bookdown)
library(knitr)
# For running bookdown - may remove this later

# Render book to html
bookdown::render_book("index.Rmd", "bookdown::gitbook")
# Serve book to see changes in real time
bookdown::serve_book()

```


```{r ,include=FALSE}
# automatically create a bib database for R packages
knitr::write_bib(c(
  .packages(), 'bookdown', 'knitr', 'rmarkdown',"DESeq2","limma","tidyverse","patchwork"
), 'packages.bib')
```

