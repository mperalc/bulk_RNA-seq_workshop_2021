# Other approaches {#other}

There are many things you can do with your bulk RNA-seq data that stem from and complement the differential expression analysis. We are listing a couple here:

## Deconvolution to identify cell types 

We can try to estimate the cell type abundances we have in our data using the bulk RNA-seq profile, particularly if we have a single cell RNA-seq reference from a closely matching tissue. By using well annotated single cell data we can just do bulk sequencing of your samples and infer the cell lines, which is particularly useful if we want to save money or go back to old data and re-analise it in ways that we couldn’t before, getting new insights.

We will use an R package called [MuSiC](https://xuranw.github.io/MuSiC/articles/MuSiC.html). MuSiC needs single cell RNA-seq data from several subject replicates that we must have previously annotated. It estimates how similar the different types are by hierarchical clustering, so we can even group together different subtypes depending on the granularity that we want. It then detects marker genes by estimating which genes are consistent across groups of cells and calculates the mean and variance using all the subjects or replicates that we have. It then upweights the genes that have lower variance as those will be more consistent across replicates, and therefore we can be more confident that they are a true cell type signature. Then it performs the deconvolution on bulk samples, calculating the cell type proportions starting first from the macro groups or clusters and then trying to estimate from those the more similar cell types.

We'll start by loading the example single cell and bulk data, both from mouse kidney. The single cell data contains samples from control and mutant mice, the latter expressing exclusively in podocytes a variant in APOL1 that [increases risk in humans for kidney disease](https://www.nature.com/articles/nm.4287). You can download both datasets [here](https://xuranw.github.io/MuSiC/articles/pages/data.html) under "Processed RNA-seq data from bulk kidney in healthy and APOL1 mouse (Beckerman et al.)" and "Subset of single cell RNA-seq data of mouse kidney (Park et al.)".

```{r}
mouse_bulk = readRDS('data/Mousebulkeset.rds')
mouse_bulk
mouse_sc = readRDS('data/Mousesubeset.rds')
mouse_sc
```
You can examine the different cell types present in the single cell data:
```{r}
unique(mouse_sc$cellType)

```
We can see the data has 16 cell types, some of which are immune cells, others are epithelial, and other are kidney-specific. It includes 2 novel cell types and what the authors call a transition cell type (CD-Trans), but we will exclude those from our analysis and focus on the better-defined cell types to avoid assignment ambiguity.

We first create for the single cell dataset the design matrix and calculate the mean and variance of relative expression abundance across all subjects, and average library size. The function `music_basis()` takes care of all of this. Its basic input variables are:

* x: The single cell dataset in `ExpressionSet` format.
* non.zero: remove all genes with zero expression accross all subjects? Default = TRUE.
* clusters: one of the column names of the phenoData from single cell data.
* samples: one of the column names of the phenoData from single cell data.
* select.ct: which clusters (cell types) to use for deconvolution. Default = NULL (use all cell types provided).

```{r}
sc.basis = music_basis(x = mouse_sc, clusters = 'cellType', samples = 'sampleID', 
                             select.ct = c('Endo', 'Podo', 'PT', 'LOH', 'DCT', 'CD-PC', 'CD-IC', 'Fib',
                                           'Macro', 'Neutro','B lymph', 'T lymph', 'NK'))

str(sc.basis)
```

The output of music_basis is a list that contains:

* Disgn.mtx: design matrix, genes as rows and cell types as columns. It contains a cell-normalised pseudobulk of counts per cluster.
* S: matrix of library sizes, samples are rows and cell type are columns.
* M.S: average library size per cell type.
* M.theta: matrix of average relative abundance, genes are rows and by cell types are columns.
* Sigma: matrix of cross-subject variation, genes are rows and by cell types are columns.

We can inspect how transcriptionally related the cell types are by performing hierarchical clustering of the cell types using the design matrix (`Disgn.mtx`):
```{r}
# Plot the dendrogram of design matrix
par(mfrow = c(1, 1))
d = dist(t(log(sc.basis$Disgn.mtx + 1e-6)), method = "euclidean")
# Hierarchical clustering using Complete Linkage
hc1 = hclust(d, method = "complete" )
# Plot the obtained dendrogram
plot(hc1, cex = 0.6, hang = -1, main = 'Cluster log(Design Matrix)')

```
You can see that the immune cell types form a distinct subgroup. We can use this to group cell types that are transcriptionally closer together, for which the classification might be less accurate. The selection of groups is a bit subjective, but it makes sense to pick a height in the dendrogram and subdivide based on that. For example, at a height of 600 we can divide the cell types into 4 clusters:

```{r}
big_clusters = list(C1 = 'Neutro', C2 = 'Podo', C3 = c('Endo', 'CD-PC', 'LOH', 'CD-IC', 'DCT', 'PT'), C4 = c('Macro', 'Fib', 'B lymph', 'NK', 'T lymph'))

cl.type = as.character(mouse_sc$cellType)
```
Now we assign these clusters to every cell in the data, ignoring the cell types we're not interested in:
```{r}

for(cl in 1:length(big_clusters)){
  cl.type[cl.type %in% big_clusters[[cl]]] = names(big_clusters)[cl]
}
pData(mouse_sc)$bigClusters = factor(cl.type, levels = c(names(big_clusters), 'CD-Trans', 'Novel1', 'Novel2'))

```
Now we need to provide differentially expressed genes from the two larger groups in the single cell dataset, to be able to separate the subgroups properly. We can do that by creating a Seurat object and performing differential expression analysis there. This will be covered in the scRNA-seq tutorial so there's no need to dwell on it now:

```{r}
metadata = Biobase::pData(mouse_sc)
counts = Biobase::exprs(mouse_sc)
mouse_seurat = Seurat::CreateSeuratObject(counts,meta.data = metadata)
mouse_seurat@meta.data$percent.mt= PercentageFeatureSet(mouse_seurat, pattern = "^MT-")

mouse_seurat = subset(mouse_seurat, subset = nFeature_RNA > 200  & percent.mt < 5)
mouse_seurat = Seurat::NormalizeData(mouse_seurat)
Idents(mouse_seurat) = "bigClusters"
diffExpr = Seurat::FindMarkers(mouse_seurat, ident.1 = "C3", ident.2 = "C4", min.pct = 0.25, test.use = "wilcox",verbose = F)

Epith.marker = diffExpr[diffExpr$p_val_adj<0.01 & diffExpr$avg_log2FC>0,]
Immune.marker = diffExpr[diffExpr$p_val_adj<0.01 & diffExpr$avg_log2FC<0,]

```
Now we can accurately estimate the cell type proportions for these similar cell types, using the function `music_prop.cluster()`. The basic input variables are:

* bulk.eset: The bulk RNA-seq dataset in `ExpressionSet` format.
* sc.eset: The annotated single cell dataset in `ExpressionSet` format.
* markers: Vector or list of gene names. Default = NULL (use all overlapping genes in bulk and single-cell dataset).
* clusters: One of the column names of the phenoData from single cell data, containing the cell types.
* samples: One of the column names of the phenoData from single cell data, containing the samples.
* groups:  One of the column names of the phenoData from single cell data, containing the higher level cluster names.
* group.markers: The list of inter-cluster differentially expressed genes calculated above.
```{r}
# We now construct the list of group marker
IEmarkers = list(NULL, NULL, rownames(Epith.marker), rownames(Immune.marker))
names(IEmarkers) = c('C1', 'C2', 'C3', 'C4')
# The name of group markers should be the same as the cluster names

estimate_bigClusters = music_prop.cluster(bulk.eset = mouse_bulk, 
                                          sc.eset = mouse_sc, 
                                          group.markers = IEmarkers, 
                                          clusters = 'cellType', 
                                          groups = 'bigClusters', 
                                          samples = 'sampleID', 
                                          clusters.type = big_clusters)

```

The results are a list of just one element, containing the cell proportion estimates. Let's have a look at the results, per sample. 

```{r}
estimate_bigClusters$Est.prop.weighted.cluster
```
The Proximal tubule cells (PT) and the Distal convoluted tubule cells (DCT) are the first and second most numerous cell type in kidney, as detected here. If you compare with the basic MuSiC function, that doesn't perform pre-clustering:
```{r}
# Estimate cell type proportions
est_prop_big_normalFunction = music_prop(bulk.eset = mouse_bulk, sc.eset = mouse_sc, 
                                         clusters = 'cellType',
                               samples = 'sampleID', 
                               select.ct = c('Endo', 'Podo', 'PT', 'LOH', 'DCT', 'CD-PC', 'CD-IC', 'Fib',
                                           'Macro', 'Neutro','B lymph', 'T lymph', 'NK'))
est_prop_big_normalFunction$Est.prop.weighted


```
You can see that due to the high similarity between DCT and PT, DCT cells are not identified at all, probably mostly being misidentified as PT. With more distinct cell types and more cells per type you could probably get away with using `music_prop()`, which as you see requires less steps, so try both with your data of interest and see if they give very different results.

Finally, you can visualize the results divided by mutation status with a scatter plot:

```{r}


long_table = est_prop_big_normalFunction$Est.prop.weighted %>% 
  as.tibble() %>%
  mutate(samples =rownames(est_prop_big_normalFunction$Est.prop.weighted),
         mutation = sub("\\..*", "", rownames(est_prop_big_normalFunction$Est.prop.weighted))) %>%
  pivot_longer(cols = !c(samples,mutation), names_to = "cell_type", values_to = "estimated_proportion")


p = ggplot(long_table, aes(x = mutation, y = estimated_proportion)) + 
  geom_point(aes(fill = mutation, color = mutation), 
             size = 2, alpha = 0.7, position = position_jitter(width=0.25, height=0)) +
  facet_wrap(~ cell_type, scales = 'free') + scale_colour_manual( values = c('red', "blue")) + theme_minimal()

p
```


## Weighted Gene Correlation Network Analysis (WGCNA)

Weighted gene correlation (or co-expression) network analysis is a way to build networks of genes based on their gene expression correlations among samples. It's usefulness is based on the idea of guilt by association: that genes that show a similar pattern of expression across groups probably perform a similar function or take part in related pathways.

WGCNA is in essence a method of dimensionality reduction. It has several applications:

 * Quality control: if a sample or a group presents a behavior that doesn’t agree with the rest it will show up in the diagnostic plots.
 * Time course analysis: it detects groups of genes that vary together with time. You can then easily identify the genes that start being expressed or peak at a certain timepoint, for example.
 * Correlation of groups of genes to other datasets: you can correlate the changes in gene expression across groups with data such as phenotypes or similarly clustered ATAC-seq peaks or protein abundances.
 
We first allow WGCNA to use as many threads as available for parallel computing, to speed up the analysis:

```{r}
allowWGCNAThreads()

```

We then read in the data, which belongs to a [study](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE149050) on the gene expression heterogeneity between two SLE subgroups (negative interferon response, INFneg, and positive, INFpos) in different immune cell types. Click [here](https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE149050&format=file&file=GSE149050%5FBulk%5FHuman%5FRawCounts%2Etxt%2Egz) to download it.

We select just a subset of the data which belongs to classical monocytes and get it into the right format. 

```{r}
counts = read.table(file = "data/GSE149050_Bulk_Human_RawCounts.txt", header = T)
# retain only a subset of classical monocytes
counts_sub = counts[,c(grep("IFNpos_cMo",colnames(counts)),grep("IFNneg_cMo",colnames(counts)))]
# get just one data point per donor - first visit
counts_sub = counts_sub[,grep("a",colnames(counts_sub))]

rownames(counts_sub) =  counts[, 1]  # gene ids are row names
metadata = data.frame(sample=colnames(counts_sub),
                      IFN_response_status = c(rep("INFpos",26),rep("INFneg",25)),
                      disease_status=c(rep("active",18),
                                       rep("inactive",8),
                                       rep("active",16),
                                       rep("inactive",9)),
                      flare_status = c("no","yes","no","yes","no",
                                       rep("yes",5),"no","yes","no",
                                       rep("yes",3),"no","yes",
                                       rep("no",14),"yes","yes",
                                       rep("no",4),"yes","no","no","yes",rep("no",9)))# save metadata information

dds = DESeq2::DESeqDataSetFromMatrix(countData = counts_sub, 
                                     colData = metadata,
                                     design = ~ IFN_response_status)
dds
```
Now we will remove the genes that have few counts across all samples. The authors recommend removing genes that have fewer than 5 normalised counts in 80% of samples to reduce the noise, and to perform variance stabilising transformation on the data.
```{r}

dds = estimateSizeFactors(dds)
keep =  rowSums(counts(dds, normalized=TRUE) > 5) > ceiling(0.8 * ncol(dds))
dds = dds[keep,]
vsd = vst(dds,blind = FALSE)

```

We are going to have a look at the data with PCA:

```{r}

getPCs = function(dds){
  
  df = assay(dds)
  pca = prcomp(t(df), retx = TRUE)
  
  percentVar = (pca$sdev)^2 / sum(pca$sdev^2)
  percentVar = round(100 * percentVar)
  pcs = as.data.frame(pca$x)
  pcs = cbind(pcs,colData(dds))
  pcs = as.data.frame(pcs)
  pcs = list(pcs, percentVar)
  
  names(pcs) = c("pcs","percentVar")
  return(pcs)
}
pcadata_vst = getPCs(vsd)

ggplot(pcadata_vst$pcs, aes(PC1, PC2, color=IFN_response_status, label = sample)) +   geom_point(size=3) +
   xlab(paste0("PC1: ",pcadata_vst$percentVar[1],"% variance")) +
   ylab(paste0("PC2: ",pcadata_vst$percentVar[2],"% variance")) + theme_bw() + geom_label_repel()
  
```
The samples look very homogeneous, except for a couple of samples. You can also see the same thing by hierarchical clustering:
```{r}
sampleTree = hclust(dist(t(assay(vsd))), method = "average")
plot(sampleTree, main = "Sample clustering to detect outliers", sub="", xlab="", cex.lab = 1.5,
    cex.axis = 1.5, cex.main = 2)

```
We will now transpose the data so that genes are in columns and samples in rows. WGCNA has a built-in function to detect outliers, `goodSamplesGenes()`, which detects samples with too many missing genes, and also genes with zero variance (these we filtered before).   

```{r}
  tdata = t(assay(vsd))
  gsg = goodSamplesGenes(tdata, verbose = 3)
  
  gsg$allOK
  # TRUE
``` 
Everything is fine (it did not flag any issues with the samples or genes), so we can proceed with this data and run WGCNA. 
\n

WGCNA starts by building a co-expression gene network where the nodes are gene profiles, and edges join correlated genes. It then calculates the adjacency, which is a measure of co-expression that we get by raising the value of the correlation to a certain power to emphasize high correlations and deflate low correlations. After that, it transforms the adjacency into the topological overlap matrix (TOM). The topological overlap is a network measure of similarity, which measures how similar two nodes (or genes) are by how many connections to other nodes they share. Two genes are said to have high topological overlap if they are connected to more or less the same group of genes in the network. 

\n
WGCNA has a built in function to calculate all these steps, `blockwiseModules()`. But before, we need to empirically choose the value of the correlation. We will choose a power value that is the lowest power for which the scale-free topology (or scale free network) fit index curve flattens out upon reaching a high value. 
an important parameter that you want to choose carefully is the type of network, which can be:

* Unsigned: it takes the absolute value of the correlation, so genes following both direct and inverse correlations will be clustered together.
* Signed: only directly correlated genes will be clustered together.
* Signed hybrid: very similar to signed, but the adjacency for genes that present negative correlations will be zero (instead of very low). 

We will pick "signed" because it usually shows good clustering behavior and it aids interpretability.

```{r}
  ### pick power
  powers = c(c(1:10), seq(from = 12, to = 20, by = 2))
  sft = pickSoftThreshold(tdata, powerVector = powers, 
                          networkType = "signed", verbose = 5)
  
  par(mfrow = c(1, 2))
  
  cex1 = 0.9
  
  plot(
    sft$fitIndices[, 1],
    -sign(sft$fitIndices[, 3]) * sft$fitIndices[, 2],
    xlab = "Soft Threshold (power)",
    ylab = "Scale Free Topology Model Fit,signed hybrid, R^2",
    type = "n",
    main = paste("Scale independence")
  )
  
  text(
    sft$fitIndices[, 1],
    -sign(sft$fitIndices[, 3]) * sft$fitIndices[, 2],
    labels = powers,
    cex = cex1,
    col = "red"
  )
  
  # this line corresponds to using an R^2 cut-off of h
  abline(h = 0.90, col = "red")
  # Mean connectivity as a function of the soft-thresholding power
  plot(
    sft$fitIndices[, 1],
    sft$fitIndices[, 5],
    xlab = "Soft Threshold (power)",
    ylab = "Mean Connectivity",
    type = "n",
    main = paste("Mean connectivity")
  )
  text(
    sft$fitIndices[, 1],
    sft$fitIndices[, 5],
    labels = powers,
    cex = cex1,
    col = "red"
  )
  
``` 
You see that the fit index curve for the scale free topology converges at a value of around 6. You generally want to see that the scale-free topology fit index reaches a value of ~0.8 for a power lower than ~30 in a signed network (for other types of networks, see the [FAQ](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/faq.html)). You also want to see that the mean connectivity (how correlated a gene is with all other network genes) falls to levels below the 100s. If not, there are probably issues with outliers driving the data, and you'll need to fix those.

Now that the have that power, we will construct the network, the adjacency matrix, and the topological overlap matrix (TOM). Again we will detail the network type ("signed") and we will use the power that we just picked. This function can be very slow if there are many genes and samples but in our case it should just take a few minutes.

```{r} 
 
power = 6

  net = blockwiseModules(
    tdata,
    power = power,
    maxBlockSize = 40000,
    networkType = "signed",
    deepSplit = 2,
    saveTOMs = T,
    saveTOMFileBase = "blockwiseTOM",
    verbose = 3
  )
 
  ##### ATTENTION: IF you get this error:
  ##### Error in (new("standardGeneric", .Data = function (x, y = NULL, use = "everything",  : 
  #####  unused arguments (weights.x = NULL, weights.y = NULL, cosine = FALSE)
  ##### do the following: 
  #in RStudio... from the "Session" pulldown, press restart R.
  #library(WGCNA).
  #Then run the blockwiseModules function again
``` 
You see how many modules (clusters) we got:
```{r} 

  sort(table(net$colors),decreasing = T)
length(unique(net$colors))
   
```
The grey module is the group of genes that WGCNA was not able to assign to any other correlation pattern. You may think that those are too many modules for the analysis that we want to make, so we can reduce this by changing a few parameters. For example, we can increase the minimum number of genes per module with `minModuleSize`, increase the `mergeCutHeight` parameter from 0.15, or decrease `deepSplit`.

\n
Now we can use a dendrogram and heatmaps to see how similar the modules are. For this we use the eigengenes of the modules, which are the first principal component of the group of genes in each module. It’s a representation of how the group of genes vary their expression across the samples.


```{r} 

  moduleLabels = unique(net$colors)
  moduleColors = net$colors
  MEs = net$MEs
  geneTree = net$dendrograms[[1]]

  # Plotting dendrogram of eigenvectors
  
  datME = moduleEigengenes(tdata, moduleColors)$eigengenes

  #########################
  

  
  par(cex = 0.9)
  plotEigengeneNetworks(
    orderMEs(datME),
    #MET,
    "",
    marDendro = c(0, 4, 1, 2),
    marHeatmap = c(3, 4, 1, 2),
    cex.lab = 0.8,
    xLabelsAngle = 90
  )
 
```
The lower in the dendrogram, the more similar the modules are. You can also see this similarity in the heatmap, which is ordered up-down and left-right in the same order than the dendrogram.

\n
You can also have a closer look at the dendrogram and see at which height we'd need to set `mergeCutHeight` in the `blockwiseModules()` function to merge however many modules you want. For example, for a height of 0.25:
```{r} 

  # Plot eigengenes with dissimilarity threshold line
  
  dissimME = (1 - WGCNA::cor(datME, method = "p")) # This is the dissimilarity value used in the 
  # function plotEigengeneNetworks
  
  hclustdatME = hclust(as.dist(dissimME), method = "average")
  
 
  par(mfrow = c(1,1))
  plot(hclustdatME,main = "Clustering of module eigengenes",xlab = "", sub = "")
  
  # Plot chosen dissimilarity value
  
  MEDissThres = 0.25
  abline(h = MEDissThres,col = "red")

```
\n 
As you can see, going back to `blockwiseModules()` and using that threshold (red line in the plot) you would merge 6 pairs of modules.

Now we can get a dataframe of the genes assigned to each module.
```{r} 

  
  gene2module = data.frame(gene = colnames(tdata), module = moduleColors)
  head(gene2module)
  
```
We can now check the patterns of expression of the samples in each module. It's better to save all of them as a PDF and then browse it, but we'll highlight a few here:
```{r} 

   ## Plotting barplots
  
  pdf(
    "data/Module_eigengenes.30M.barplots.pdf",
    width = 10,
    height = 3
  )
p = list()
  for (module in unique(moduleColors)) {
    ME = datME[, paste("ME", module, sep = "")]
    ME = data.frame(names = rownames(datME),eigengene = ME)
    
    p[[module]] = ggplot(data=ME, aes(x=names, y=eigengene)) +
      geom_bar(stat="identity") + 
      theme_bw() + 
      ylab("eigengene expression") + 
      xlab("array sample") + 
      ggtitle(paste0(module," - ",  table(gene2module$module)[module], " genes")) +
      theme(axis.text.x = element_text(angle = 90))
    print(p[[module]])
  }
  dev.off()
  
  
print(p[["pink"]])
```
\n
You can clearly see here a big outlier, one of the samples that appeared further away in the PCA plot. 
```{r} 
print(p[["blue"]])

```
\n
You can see in this large module that the expression goes up and down without an apparent pattern. Maybe there's an underlying cause we are not aware of. To study this is helpful to correlate the module eigengenes to the metadata.

We'll first code the categorical variables we have in a binary format:
```{r} 
# Define numbers of genes and samples
nGenes = ncol(tdata)
nSamples = nrow(tdata)
binarised_metadata = data.frame(IFN_response_status = binarizeCategoricalVariable(metadata$IFN_response_status),
                                disease_status = binarizeCategoricalVariable(metadata$disease_status,levelOrder = c("inactive","active")),
                                flare_status = binarizeCategoricalVariable(metadata$flare_status))
```
Now we can correlate gene expression with these variables, and calculate a p-value for the correlation:

```{r}
moduleTraitCor = WGCNA::cor(MEs, binarised_metadata, use = "p")
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples)
```
We can now plot these in a heatmap:
```{r}
sizeGrWindow(15,8)
# Will display correlations and their p-values
textMatrix = paste(signif(moduleTraitCor, 2), "\n(",
                        signif(moduleTraitPvalue, 1), ")", sep = "");
dim(textMatrix) = dim(moduleTraitCor)
par(mar = c(6, 8.5, 3, 3));
# Display the correlation values within a heatmap plot
labeledHeatmap(Matrix = moduleTraitCor,
             xLabels = colnames(metadata)[-1],
             yLabels = names(MEs),
             ySymbols = names(MEs),
             colorLabels = FALSE,
             colors = greenWhiteRed(50),
             textMatrix = textMatrix,
             setStdMargins = FALSE,
             cex.text = 0.5,
             zlim = c(-1,1),
             main = paste("Module-trait relationships"))


```
```{r , out.width='90%', fig.asp=.75, fig.align='center', echo=F}
img1_path <- "data/figures/MEheatmap.png"
include_graphics(img1_path)

```

You can see that one module (green) stands out as having large direct correlation with the IFN response status. You can see from the barplot of the module eigengenes how this makes sense:
```{r}
print(p[["green"]])

```
There's also strong inverse correlation with the lightgreen module:
```{r}
print(p[["lightgreen"]])

```
This analysis gives a set of genes correlated with your trait of interest for further investigation. You can look at this in finer detail, for example analysing [how well each gene is connected (correlated) to the other genes within each module](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/Simulated-07-Membership.pdf), [alternative network visualizations](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/Simulated-08-Visualization.pdf) and more. You can find the full list of tutorials [here](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/).